#ifndef BUSCA_EM_LARGURA_H
#define BUSCA_EM_LARGURA_H

#include <stdlib.h>
#include "fila/fila.h"

// Guarda a ordem de visitação dos vertives do grafo
int num[1000];

struct node_grafo {
   int dado;						// dado guardado
   struct node_grafo *proximo;		// ponteiro para o proximo nó
};

typedef struct node_grafo *link;

struct grafo {
   int vertices; 	// Quantidade de vertices
   int arestas; 	// Quantidade de arestas
   link *adj;		// Lista de adjacencias
};

typedef struct grafo *Grafo;	// ponteiro para um grafo

Grafo inicializar_grafo(int v);
static link novo_no(int dado, link proximo);
void adicionarAresta(Grafo g, int origem, int destino);
void imprimirGrafo(Grafo g);
void bfs(Grafo G, int s);
void destruir_grafo(Grafo g);

#endif
