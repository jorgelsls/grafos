# grafos

## Clonando o repositorio git

```bash
git clone https://github.com/lopesluisjorge/grafos.git
cd grafos
```

## Adicionando as mudanças no repositório 

- Para adicionar um arquivo especifico
```bash
git add nomedoarquivo.extensao
```

- Para adicionar todos os arquivos
```bash
git add .
```

## Fazendo commit das mudanças

```bash
git commit -m "mensagem com mudanças realizadas"
```

## Subindo as mudanças para o repositório remoto

```bash
git push
```
