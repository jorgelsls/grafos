#include "fila.h"

fila *inicializar_fila(void) {
	fila *_fila = (fila *) malloc(sizeof(fila));

	if (_fila != NULL) {
		_fila->inicio = NULL;
		_fila->fim   = NULL;
		_fila->tamanho  = 0;
	}

	return _fila;
}


int tamanho_fila(fila *_fila) {
	if (_fila != NULL) {
		return _fila->tamanho;
	}

	return -1;
}

int empty_fila(fila *_fila) {
	if (_fila != NULL) {
		return _fila->tamanho == 0 && _fila->inicio == NULL;
	}

	return -1;
}

fila *adicionar_fila(fila *_fila, int dado) {
	if (_fila == NULL) {
		printf("fila does not exist\n");
		return NULL;
	}

	node *new_node = (node *) (malloc(sizeof(node)));

	if(new_node == NULL){
		printf("Create fila error\n");
		return _fila;
	}

	new_node->dado = dado;
	new_node->proximo = NULL;

	if (_fila->inicio == NULL) {
		_fila->inicio = new_node;
		_fila->fim = new_node;
	} else {
	_fila->fim->proximo = new_node;
	_fila->fim = new_node;
	}

	_fila->tamanho++;

	return _fila;
}

int inicio_fila(fila *_fila) {
	if (_fila == NULL) {
		printf("fila does not exist\n");
		return 0;
	}

	if (empty_fila(_fila)) {
		printf("fila underflow");
		return -1;
	}

	return _fila->inicio->dado;
}

int retirar_fila(fila *_fila) {
	if (_fila == NULL) {
		printf("fila does not exist\n");
		return 0;
	}

	if (empty_fila(_fila)) {
		printf("fila underflow\n");
		return -1;
	}

	node *atual = _fila->inicio;
	_fila->inicio  = atual->proximo;
	_fila->tamanho  -= 1;

	int atual_inicio_value = atual->dado;

	free(atual);

	return atual_inicio_value;
}

void destruir_fila(fila *_fila) {
	if (_fila == NULL) {
		return;
	}

	while (!empty_fila(_fila)) {
		retirar_fila(_fila);
	}

	free(_fila);
	_fila = NULL;
}
