#include "BuscaEmProfundidade.h"

int main() {
	int v = 8;

	Grafo grafo = inicializar_grafo(v);

	// adicionando as arestas
	adicionarAresta(grafo, 0, 1);
	adicionarAresta(grafo, 0, 2);
	adicionarAresta(grafo, 1, 3);
	adicionarAresta(grafo, 1, 4);
	adicionarAresta(grafo, 2, 5);
	adicionarAresta(grafo, 2, 6);
	adicionarAresta(grafo, 6, 7);

	int visitados[10];
	buscaProfundidade(grafo);

	imprimirGrafo(grafo);

	int i;
	for (i = 0; i < grafo->total+1; i++) {
		printf("%d ", pre[i]);
	}

	liberar_grafo(grafo);

	return 0;
}

Grafo inicializar_grafo(int vertices) {
	Grafo g = (Grafo) malloc(sizeof *g);
	g->vertices = vertices;
	g->arestas = 0;
	g->adj = inicializar_matriz(vertices, vertices, 0);
	g->total = 0;

	return g;
}

static int **inicializar_matriz(int linhas , int colunas, int valor) {
    int **matriz = (int **) malloc(linhas * sizeof (int *));

	int i, j;
	for (i = 0; i < linhas ; ++i) {
		matriz[i] = (int *) malloc(colunas * sizeof (int));
	}
	for (i = 0; i < linhas ; ++i) {
		for (j = 0; j < colunas; ++j) {
			matriz[i][j] = valor;
		}
	}

	return matriz;
}

 // adiciona uma aresta no grafo
void adicionarAresta(Grafo g, int origem, int destino) {
	if (g == NULL) {
		return;
	}
	if (origem < 0 || origem >= g->vertices) {
		return;
	}
	if (destino < 0 || destino >= g->vertices) {
		return;
	}

    if (g->adj[origem][destino] == 0) {
		g->adj[origem][destino] = 1;
		g->arestas++;
		g->total++;
	}
}

void imprimirGrafo(Grafo g) {
	int i, j;

	for (i = 0; i < g->vertices; i++) {
		printf("Vertice %d: ", i);
		for (j = 0; j < g->vertices; j++) {
			printf("%d, ", g->adj[i][j]);
		}
		printf("\n");
	}
}

void buscaProfundidade(Grafo g) {
	cont = 0;

	int vertice;
	for (vertice = 0; vertice < g->vertices; ++vertice) {
		pre[vertice] = -1;
	}
	for (vertice = 0; vertice < g->vertices; ++vertice) {
		if (pre[vertice] == -1) {
			dfs(g, vertice); // começa nova etapa
		}
	}
}

static void dfs(Grafo g, int v) {
	pre[v] = cont++;
	int w;
	for (w = 0; w < g->vertices; ++w) {
		if (g->adj[v][w] != 0 && pre[w] == -1) {
			dfs(g, w);
		}
	}
}

void liberar_grafo(Grafo g) {
	if (g == NULL) {
		return;
	}

	int i;
	for (i = 0; i < g->vertices; ++i) {
		free(g->adj[i]);
	}

	free(g->adj);
	g->adj = NULL;
	free(g);
	g = NULL;
}
